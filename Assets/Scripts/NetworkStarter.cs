using Unity.Netcode;
using UnityEngine;

public class NetworkStarter : MonoBehaviour
{   
    // Funkcje laczace, host/server tworzy server, klient laczy sie z serverem
    public void StartServer(){
        NetworkManager.Singleton.StartServer();

    }
     public void StartHost(){
        NetworkManager.Singleton.StartHost();

    }
     public void StartClient(){
        NetworkManager.Singleton.StartClient();

    }

    
}
