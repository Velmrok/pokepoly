using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using Unity.Networking;
using TMPro;
using UnityEngine.UI;
public class ManageInventories : NetworkBehaviour
{
    public string[] pokename = new string[809];
    PlayersInventories pi;
    [SerializeField] GameObject InventoryPanel;
    [SerializeField] GameObject PokemonManager;
    PokemonArray pa;
    
    [SerializeField] TextMeshProUGUI PokemonText;
    [SerializeField] TextMeshProUGUI[] PokemonTextList;
    [SerializeField] TextMeshProUGUI pokeballtext;
    [SerializeField] GameObject CanvasManagerObject;

    [SerializeField] GameObject PokeDetailPanel;
    [SerializeField] TextMeshProUGUI PokeDetailName;
    [SerializeField] Image PokeDetailImage;
    LoadSprites ls;
    int globaliterator = 0;
    void Start(){
        InventoryPanel.SetActive(false);
        ls = CanvasManagerObject.GetComponent<LoadSprites>();
        pa = PokemonManager.GetComponent<PokemonArray>(); 
        pi = GetComponent<PlayersInventories>();
        for (int i = 0; i < 721; i++)
        {
            pokename[i] = pa.PokemonList.PokemonList[i].name;
        }
    }
    void Update(){
        RefreshEQServerRpc();
      
    }

    public void OnButtonCloseOpenEQ()
    {
        if (InventoryPanel.gameObject.activeSelf)InventoryPanel.SetActive(false);
        else InventoryPanel.SetActive(true);
      

    }
    [ServerRpc(RequireOwnership =false)]
    void RefreshEQServerRpc(){
        RefreshEQClientRpc();
    }
    [ClientRpc]
    void RefreshEQClientRpc(){
        if (pi.PlayerInventory != null && pi.PlayerInventory.Count > 0)
        {


            if (pi.PlayerInventory.TryGetValue((int)NetworkManager.Singleton.LocalClientId,out var playerseq))
            {
                
                pokeballtext.text = "Ilość pokeballi " + playerseq.NumberOfPokeballs;
                PokemonText.text = $"Twoje pokemony {playerseq.PokemonList.Count}/8\n";
                for (int i =0; i < playerseq.PokemonList.Count; i++)
                {
                    var pokemon = playerseq.PokemonList[i];
                    string type = "";
                    foreach(var _type in pokemon.type){
                        string temptype = SetTypeColor(_type);
                        type += temptype + " ";
                    }
                    
                    //type = SetTypeColor(type);
                
                   
                    PokemonTextList[i].text = $"<color=red> - {pokename[pokemon.id-1]}</color> <color=green>{pokemon.HP}/{pokemon.Base.HP}HP</color> {type}\n";

                   
                }
            }
        }
    }


    public void OnButtonShowMore1() {
        if(PokeDetailPanel.activeSelf)PokeDetailPanel.SetActive(false);
        else PokeDetailPanel.SetActive(true);
        PokeDetailImage.sprite = ls.SpriteList[pi.PlayerInventory[(int)NetworkManager.Singleton.LocalClientId].PokemonList[0].id-1];
     }
    public void OnButtonShowMore2() { 
         if(PokeDetailPanel.activeSelf)PokeDetailPanel.SetActive(false);
        else PokeDetailPanel.SetActive(true);
        PokeDetailImage.sprite = ls.SpriteList[pi.PlayerInventory[(int)NetworkManager.Singleton.LocalClientId].PokemonList[1].id-1];
    }
    public void OnButtonShowMore3() { 
         if(PokeDetailPanel.activeSelf)PokeDetailPanel.SetActive(false);
        else PokeDetailPanel.SetActive(true);
        PokeDetailImage.sprite = ls.SpriteList[pi.PlayerInventory[(int)NetworkManager.Singleton.LocalClientId].PokemonList[2].id-1];
    }
    public void OnButtonShowMore4() { 
         if(PokeDetailPanel.activeSelf)PokeDetailPanel.SetActive(false);
        else PokeDetailPanel.SetActive(true);
        PokeDetailImage.sprite = ls.SpriteList[pi.PlayerInventory[(int)NetworkManager.Singleton.LocalClientId].PokemonList[3].id-1];
    }
    public void OnButtonShowMore5() { 
         if(PokeDetailPanel.activeSelf)PokeDetailPanel.SetActive(false);
        else PokeDetailPanel.SetActive(true);
        PokeDetailImage.sprite = ls.SpriteList[pi.PlayerInventory[(int)NetworkManager.Singleton.LocalClientId].PokemonList[4].id-1];
    }
    public void OnButtonShowMore6() { 
         if(PokeDetailPanel.activeSelf)PokeDetailPanel.SetActive(false);
        else PokeDetailPanel.SetActive(true);
        PokeDetailImage.sprite = ls.SpriteList[pi.PlayerInventory[(int)NetworkManager.Singleton.LocalClientId].PokemonList[5].id-1];
    }
    public void OnButtonShowMore7() { 
         if(PokeDetailPanel.activeSelf)PokeDetailPanel.SetActive(false);
        else PokeDetailPanel.SetActive(true);
        PokeDetailImage.sprite = ls.SpriteList[pi.PlayerInventory[(int)NetworkManager.Singleton.LocalClientId].PokemonList[6].id-1];
    }   
    public void OnButtonShowMore8() { 
         if(PokeDetailPanel.activeSelf)PokeDetailPanel.SetActive(false);
        else PokeDetailPanel.SetActive(true);
        PokeDetailImage.sprite = ls.SpriteList[pi.PlayerInventory[(int)NetworkManager.Singleton.LocalClientId].PokemonList[7].id];
    }


    public string SetTypeColor(string type){
        switch(type){
            case "Normal":
                type = $"<color=#ffa500>{type}</color>";
                break;
            case "Dragon":
                type = $"<color=#ffa500>{type}</color>";
                break;
            case "Water":
                type = $"<color=#0099CC>{type}</color>";
                break;  
            case "Ice":
                type = $"<color=#0099CC>{type}</color>";
                break;
            case "Grass":
                type = $"<color=#00CC33>{type}</color>";
                break;
            case "Bug":
                type = $"<color=#339933>{type}</color>";
                break;
            case "Fire":
                type = $"<color=#FF3333>{type}</color>";
                break;
            case "Rock":
                type = $"<color=#AAA>{type}</color>";
                break;
            case "Ghost":
                type = $"<color=#000>{type}</color>";
                break;
            case "Electro":
                type = $"<color=#FFFF00>{type}</color>";
                break;
            case "Poison":
                type = $"<color=#990099>{type}</color>";
                break;
            case "Psycho":
                type = $"<color=#CC3399>{type}</color>";
                break;
            case "Ground":
                type = $"<color=#996633>{type}</color>";
                break;
            case "Fighting":
                type = $"<color=#C0C0C0>{type}</color>";
                break;
            default:
                break;

        }
        return type;
    }


}
