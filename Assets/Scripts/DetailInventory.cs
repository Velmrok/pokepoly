using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class DetailInventory : NetworkBehaviour
{
    [SerializeField] GameObject CanvasManager;
    [SerializeField] GameObject DetailEQ;
    [SerializeField] GameObject[] DetailCardArray;
    [SerializeField] Image[] ImageArray;
    [SerializeField] TextMeshProUGUI[] PokeNameArray;
    [SerializeField] TextMeshProUGUI[] TypesArray;
    [SerializeField] TextMeshProUGUI[] StatsArray;
    PlayersInventories pi;
    LoadSprites ls;
    ManageInventories mi;

    void Start(){
        pi = GetComponent<PlayersInventories>();
        ls = CanvasManager.GetComponent<LoadSprites>();
        mi = GetComponent<ManageInventories>();
        DetailEQ.SetActive(false);
        for (int i = 0; i < 8; i++)
        {
            DetailCardArray[i].SetActive(false);
        }
    }



    public void OnButtonOpenEQ()
    {
        if (!DetailEQ.activeSelf)
        {
             DetailEQ.SetActive(true);
            int i = 0;
            foreach (var pokemon in pi.PlayerInventory[(int)NetworkManager.Singleton.LocalClientId].PokemonList)
            {
                ImageArray[i].sprite = ls.SpriteList[pokemon.id - 1];
                PokeNameArray[i].text = mi.pokename[pokemon.id - 1];
                foreach (var type in pokemon.type)
                {
                    string _type = mi.SetTypeColor(type);
                    TypesArray[i].text += _type + " ";
                }
                StatsArray[i].text = $" <color=#FF3399>HP {pokemon.HP}/{pokemon.Base.HP}</color>\n" +
                        $"<color=#FF0000>ATK {pokemon.Base.Attack}</color>\n"+
                        $"<color=#00CC33>DEF {pokemon.Base.Defense}</color>";

                DetailCardArray[i].SetActive(true);
                i++;
            }
        }
    }
    public void OnButtonCloseEQ(){
        if (DetailEQ.activeSelf)
        {
            DetailEQ.SetActive(false);
            int i = 0;
            foreach (var card in DetailCardArray)
            {
                TypesArray[i].text = "";
                card.SetActive(false);
                i++;
            }
        }
    }
}
