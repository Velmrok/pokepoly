using System.Data.Common;
using System.Threading.Tasks;
using TMPro;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;

public class MoneyManager : NetworkBehaviour
{
    [SerializeField] TextMeshProUGUI moneyscreentext;
    [SerializeField] TextMeshProUGUI ChangedMoneyScreenText;
    [SerializeField] TextMeshProUGUI PlayerLoseText;
    [SerializeField] GameObject Dice;
    [SerializeField] GameObject CanvasManager;
    FieldStatus fs;
    Dice dicecomp;

   int[] money = { 1000, 1000, 1000, 1000 };
    ulong localid = 0;
    void Start(){
        fs = CanvasManager.GetComponent<FieldStatus>();
        dicecomp = Dice.GetComponent<Dice>();
    }
    
    void Update()
    {
        for (int i = 0; i < money.Length; i++)
        {
            if(money[i]<0){
                PlayerLose(i);
            }
        }
        localid = NetworkManager.Singleton.LocalClientId;

        moneyscreentext.text = "Twój hajs: " + money[localid] + "€";
    }
    [ClientRpc]
    public void ChangePlayerMoneyClientRpc(int id, int value)
    {
        money[id] += value;
        if(NetworkManager.Singleton.LocalClientId==(ulong)id) DisplayChangedMoney(value);
       
    
    }
    public int GetMoney(int id){
        return money[id];
    }
     public void PayIfNeeded(int value, string towho,FieldStatus fieldStatus,string Tag){
        if (towho == "Ticket")
        {
            ChangePlayerMoneyServerRpc((ulong)dicecomp.tempplayersarrayvalue.Value, value);
        }
        else
        {
            ulong playernumber = ulong.Parse(fieldStatus.FieldsStatutes[Tag].Substring(12)) - 1;
           
            if (playernumber != (ulong)dicecomp.tempplayersarrayvalue.Value)
            {

                if (towho == "Bought")
                {
                    ChangePlayerMoneyServerRpc((ulong)dicecomp.tempplayersarrayvalue.Value, value);
                    ChangePlayerMoneyServerRpc(playernumber, -value);
                }


            }
        }
    }
      [ServerRpc(RequireOwnership =false)]
    public void ChangePlayerMoneyServerRpc(ulong playernumber, int value){
        ChangePlayerMoneyClientRpc((int)playernumber, value);
    }
    async void PlayerLose(int id){
        PlayerLoseText.text = "Gracz" + (id + 1) + " odpadł!";
        PlayerLoseText.gameObject.SetActive(true);
        await Task.Delay(3000);
        money[id] = 0;
         //GameObject PlayerThatLost = GameObject.Find("Player"+(id + 1));
          GameObject[] AllGameObjects= FindObjectsOfType<GameObject>();
          foreach(var Object in AllGameObjects){
            if (Object.name.Equals("Player"+(id + 1))) Object.gameObject.SetActive(false);
            else if(Object.name.StartsWith("Player"+(id + 1)))Destroy(Object);
            
           
          }

        PlayerLoseText.gameObject.SetActive(false);
        dicecomp.GetLostPlayer(id);
        fs.ClearAllLostPlayerStatuses(id);
        
    }
    async void DisplayChangedMoney(int value)
    {
        ChangedMoneyScreenText.color = value < 0 ? Color.red : Color.green;
        ChangedMoneyScreenText.text = value < 0 ?value + "€":"+"+value + "€";
        await Task.Delay(1500);
        ChangedMoneyScreenText.text = "";
    }


}