
using System.Collections.Generic;
using Unity.Netcode;

public class PlayersInventories : NetworkBehaviour
{
    public Dictionary<int, Inventory> PlayerInventory = new Dictionary<int, Inventory>();


     public void AddPlayer(int playerId)
    {
        PlayerInventory.Add(playerId, new Inventory());
    }
}