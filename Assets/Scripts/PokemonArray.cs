using System.Collections.Generic;
using System.IO;
using Unity.Netcode;
using UnityEngine;
using Newtonsoft.Json;

public class PokemonArray : NetworkBehaviour
{
   internal PokemonListContainer PokemonList;
   internal TempPokemonListContainer TempPokemonList;
    public Dictionary<string, string> FieldType;
    public Dictionary<string, List<Pokemon>> PokemonOnFields;

    RandomPokemon rp;
    
    void Start()
    {
        rp = GetComponent<RandomPokemon>();
        PokemonOnFields = rp.SynchronizedListOfPoksOnFields;
        PokemonList = new PokemonListContainer();
        PokemonList.PokemonList = new List<Pokemon>();
        if(TempPokemonList is null){
            
            // string PokemojListjson = File.ReadAllText("Assets/DateBase/pokedex.json");
            // PokemonList = JsonUtility.FromJson<PokemonListContainer>(PokemojListjson);
            // Debug.Log("Udalo sie wczytac " + PokemonList.PokemonList.Count + " pokemonow");
           
             string PokemojListjson = File.ReadAllText(Application.streamingAssetsPath+"/DateBase/DateBase/pokedex.json");
           TempPokemonList = JsonConvert.DeserializeObject<TempPokemonListContainer>(PokemojListjson);
           Debug.Log("Udalo sie wczytac " + TempPokemonList.PokemonList.Count + " pokemonow");
            
            foreach (var pokemon in TempPokemonList.PokemonList)
            {
                Pokemon actualpokemon = new Pokemon();
                actualpokemon.Base = new Base();
                    actualpokemon.id = pokemon.id;
                    
                   
                    actualpokemon.Base.Attack = pokemon.Base.Attack;
                    actualpokemon.Base.Defense = pokemon.Base.Defense;
                    actualpokemon.Base.HP = pokemon.Base.HP;
                    actualpokemon.name = pokemon.name["english"];
                    actualpokemon.type = pokemon.type;
                
                    PokemonList.PokemonList.Add(actualpokemon);
                
            }
             foreach (var pokemon in PokemonList.PokemonList)
            {
                pokemon.Attack = pokemon.Base.Attack;
                pokemon.HP = pokemon.Base.HP;
                pokemon.Defense = pokemon.Base.Defense;
            }
             Debug.Log("Udalo sie wczytac " + PokemonList.PokemonList.Count + " pokemonow");
            Debug.Log(Application.streamingAssetsPath);
           

        }

        string FieldTypejson = File.ReadAllText(Application.streamingAssetsPath+"/DateBase/DateBase/FieldTypes.json");
        FieldType = JsonConvert.DeserializeObject<Dictionary<string, string>>(FieldTypejson);
        Debug.Log("Udalo sie wczytac " + FieldType.Count+ " typow pol");
        
            


        

       
    }


}
