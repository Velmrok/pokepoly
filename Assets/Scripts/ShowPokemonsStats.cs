using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TMPro;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class ShowPokemonsStats:NetworkBehaviour
{
    string OnPointerObjectName;
    public Image PokemonImage;
    public List<GameObject> InventoryTriggerPanels;
    [SerializeField] GameObject PokemonDetailsPanel;
    [SerializeField] GameObject PokemonManagerObject;
    [SerializeField] TextMeshProUGUI PokeName;
    [SerializeField] TextMeshProUGUI PokeTypes;
    [SerializeField] TextMeshProUGUI PokeStats;
    [SerializeField] GameObject InventoryObject;
    PlayersInventories playersInventories;
    ManageInventories mi;
    PokemonArray pa;
    FieldCanvas fc;
    
    void Start()
    {
        Debug.Log("");
        PokemonDetailsPanel.SetActive(false);
        fc = GetComponent<FieldCanvas>();
        pa = PokemonManagerObject.GetComponent<PokemonArray>();
        playersInventories = InventoryObject.GetComponent<PlayersInventories>();
        mi = InventoryObject.GetComponent<ManageInventories>();
    }

    public void  OnPointerEnter1(){
        Pokemon DisplayedPokemon = pa.PokemonOnFields[fc.Tag][0];
        PokemonImage.sprite = fc.PokeImage[0].sprite;
        PokeName.text = DisplayedPokemon.name;
        PokeStats.text = $" <color=#FF3399>HP  {DisplayedPokemon.Base.HP}</color>\n" +
                        $"<color=#FF0000>ATK {DisplayedPokemon.Base.Attack}</color>\n"+
                        $"<color=#00CC33>DEF {DisplayedPokemon.Base.Defense}</color>";

        if (PokeTypes.text == "")
        {
            foreach (var _type in DisplayedPokemon.type)
            {
                string temptype = mi.SetTypeColor(_type);
                PokeTypes.text += temptype + " ";
            }
        }
        PokemonDetailsPanel.SetActive(true);

    }

    /// ///////////////////////////////////////////
     public void  OnPointerEnter2(){
        Pokemon DisplayedPokemon = pa.PokemonOnFields[fc.Tag][1];
        PokeName.text = DisplayedPokemon.name;

        PokemonImage.sprite = fc.PokeImage[1].sprite;

         PokeStats.text = $" <color=#FF3399>HP  {DisplayedPokemon.Base.HP}</color>\n" +
                        $"<color=#FF0000>ATK {DisplayedPokemon.Base.Attack}</color>\n"+
                        $"<color=#00CC33>DEF {DisplayedPokemon.Base.Defense}</color>";

        if (PokeTypes.text == "")
        {
            foreach (var _type in DisplayedPokemon.type)
            {
                string temptype = mi.SetTypeColor(_type);
                PokeTypes.text += temptype + " ";
            }
        }
        PokemonDetailsPanel.SetActive(true);
    }
    /// ///////////////////////////////////////////
     public void  OnPointerEnter3(){
       Pokemon DisplayedPokemon = pa.PokemonOnFields[fc.Tag][2];
        PokeName.text = DisplayedPokemon.name;

        PokemonImage.sprite = fc.PokeImage[2].sprite;
         PokeStats.text = $" <color=#FF3399>HP  {DisplayedPokemon.Base.HP}</color>\n" +
                        $"<color=#FF0000>ATK {DisplayedPokemon.Base.Attack}</color>\n"+
                        $"<color=#00CC33>DEF {DisplayedPokemon.Base.Defense}</color>";

        if (PokeTypes.text == "")
        {
            foreach (var _type in DisplayedPokemon.type)
            {
                string temptype = mi.SetTypeColor(_type);
                PokeTypes.text += temptype + " ";
            }
        }
        PokemonDetailsPanel.SetActive(true);
    }

          public void  OnPointerExit(){
        PokeTypes.text = "";
        PokemonDetailsPanel.SetActive(false);
    }
}