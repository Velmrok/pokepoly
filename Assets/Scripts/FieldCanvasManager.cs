using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using TMPro;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using ColorUtility = UnityEngine.ColorUtility;

public class FieldCanvas : NetworkBehaviour
{
     [SerializeField]private TextMeshProUGUI titletext;
     [SerializeField]private TextMeshProUGUI OwnerText;
    [SerializeField] private TextMeshProUGUI InfoText;
    [SerializeField] private TextMeshProUGUI PlayerPopText;
    public  TextMeshProUGUI Pokemon1;
    public  TextMeshProUGUI Pokemon2;
    public  TextMeshProUGUI Pokemon3;
    [SerializeField] private Button Buybutton;
    [SerializeField] private Button Cancelbutton;
    [SerializeField] private Button Paybutton;
    [SerializeField] private Button Continuebutton;
    [SerializeField] private Button OpenPokeMenubutton;
    [SerializeField] private Button HealPokemonsbutton;
    [SerializeField] private Button BuyPokeballsbutton;
    [SerializeField] GameObject PokemonArrayConnect;
    [SerializeField] GameObject canvas;
    [SerializeField] GameObject Globalcanvas;
    [SerializeField] GameObject Diceobj;
    [SerializeField] GameObject PanelToBuyPokemons;
    [SerializeField] GameObject PanelInteractWithField;
    [SerializeField] GameObject InventoryObject;
    [SerializeField] GameObject PokeCard1;
    [SerializeField] GameObject PokeCard2;
    [SerializeField] GameObject PokeCard3;
    [SerializeField] GameObject[] TriggerPanel;
    public  Image[] PokeImage;
    private PlayerMovementNT pm;
    ChanceChallenge cc;
    LoadSprites ls;
    PlayersInventories inv;
    FieldStatus fieldStatus;
   MoneyManager mm;
    private Dice dc;
   private PokemonArray pa;
   int reward;
    [DoNotSerialize] public string Tag;
    public NetworkVariable<int> TagNumber;
   

     void Start(){
        
        reward = -1;
       
        cc = Diceobj.GetComponent<ChanceChallenge>();
        inv = InventoryObject.GetComponent<PlayersInventories>();
        fieldStatus = GetComponent<FieldStatus>();
        ls = GetComponent<LoadSprites>();
        mm = Globalcanvas.GetComponent<MoneyManager>();
        dc =  Diceobj.GetComponent<Dice>();
        pm =  Diceobj.GetComponent<PlayerMovementNT>();
        pa = PokemonArrayConnect.GetComponent<PokemonArray>();
        canvas.SetActive(false);
        PanelToBuyPokemons.SetActive(false);
        OpenPokeMenubutton.gameObject.SetActive(false);
        foreach(var panel in TriggerPanel){
            panel.SetActive(false);
        }
        
     }
     void Update()
     {
        TakeTag();
        
        ChangeWindowTitleClientRpc();
        ChangeBuyingPanelClientRpc();
       
     }
     
     public void TakeTag(){
        Tag = "P" + TagNumber.Value;
        
    }
    
    public void ShowCanvas(byte id, ulong localid)
    {

      
       
        if(id==localid&&!pm.isWalking.Value&&!dc.playersThatLost[localid])Invoke("ShowCanvasAfterDelay", 0.3f);



    }
  
    [ClientRpc]
    void ChangeBuyingPanelClientRpc()
    {
        if (pa.PokemonOnFields.ContainsKey(Tag))
        {   if (pa.PokemonOnFields[Tag][0].name == "DELETED" && pa.PokemonOnFields[Tag][1].name == "DELETED" && pa.PokemonOnFields[Tag][2].name == "DELETED")
            {
                for (int i = 0; i < 3; i++)
                {
                    TriggerPanel[i].SetActive(false);
                }
                PanelInteractWithField.SetActive(true);
                PanelToBuyPokemons.SetActive(false);
                Buybutton.gameObject.SetActive(false);
                
                Cancelbutton.gameObject.SetActive(false);
                ulong playernumber = ulong.Parse(fieldStatus.FieldsStatutes[Tag].Substring(12));
                 playernumber--;
                 if(NetworkManager.Singleton.LocalClientId==playernumber)Continuebutton.gameObject.SetActive(true);
            }
            else
            {
                if (pa.PokemonOnFields[Tag][0].name == "DELETED")
                {
                    TriggerPanel[0].SetActive(false);
                    PokeCard1.SetActive(false);
                }
                else
                {
                    if(PanelToBuyPokemons.activeSelf)TriggerPanel[0].SetActive(true);
                    else TriggerPanel[0].SetActive(false);
                    SetupPokeCardImage(0, pa.PokemonOnFields[Tag][0].id);
                    PokeCard1.SetActive(true);
                    Pokemon1.text = pa.PokemonOnFields[Tag][0].name;
                }
                if (pa.PokemonOnFields[Tag][1].name == "DELETED")
                {
                    TriggerPanel[1].SetActive(false);
                    PokeCard2.SetActive(false);
                }
                else
                {
                    if(PanelToBuyPokemons.activeSelf)TriggerPanel[1].SetActive(true);
                    else TriggerPanel[1].SetActive(false);
                    SetupPokeCardImage(1, pa.PokemonOnFields[Tag][1].id);
                    PokeCard2.SetActive(true);
                    Pokemon2.text = pa.PokemonOnFields[Tag][1].name;
                }
                if (pa.PokemonOnFields[Tag][2].name == "DELETED")
                {
                    TriggerPanel[2].SetActive(false);
                    PokeCard3.SetActive(false);
                }
                else
                {
                   if(PanelToBuyPokemons.activeSelf) TriggerPanel[2].SetActive(true);
                   else TriggerPanel[2].SetActive(false);
                    SetupPokeCardImage(2, pa.PokemonOnFields[Tag][2].id);
                    PokeCard3.SetActive(true);
                    Pokemon3.text = pa.PokemonOnFields[Tag][2].name;
                }

            }

        }
    }
    void SetupPokeCardImage(int i, int pokeid){



        Sprite sprite = ls.SpriteList[pokeid - 1];
            PokeImage[i].sprite = sprite;
       
    }
    public void OnButtonOpenPokeMenu(){
         PanelInteractWithField.SetActive(false);
        PanelToBuyPokemons.SetActive(true);
    }
      public void OnButtonClosePokeMenu(){
        Buybutton.gameObject.SetActive(false);
         PanelInteractWithField.SetActive(true);
        PanelToBuyPokemons.SetActive(false);
    }
    public void OnButtonBuyPokemon1(){
        int playeriterator = dc.tempplayersarrayvalue.Value;
        if(inv.PlayerInventory[playeriterator].NumberOfPokeballs==0){
           PlayerPopText.text = "Brakuje ci pokeballi!";
            WaitThenClearPopText(1000);
        }else if(mm.GetMoney(playeriterator) - 50 < 0){
            PlayerPopText.text = "Nie stać cie na kupno!";
            WaitThenClearPopText(1000);
        }else{
            BuyPokemonServerRpc(0);
            inv.PlayerInventory[playeriterator].AddPokeballClientRpc(-1);
        }
        
    }
    public void OnButtonBuyPokemon2(){
        int playeriterator = dc.tempplayersarrayvalue.Value;
          if(inv.PlayerInventory[playeriterator].NumberOfPokeballs==0){
           PlayerPopText.text = "Brakuje ci pokeballi!";
            WaitThenClearPopText(1000);
        }else if(mm.GetMoney(playeriterator) - 50 < 0){
            PlayerPopText.text = "Nie stać cie na kupno!";
            WaitThenClearPopText(1000);
        }
        else{
            BuyPokemonServerRpc(1);
            inv.PlayerInventory[playeriterator].AddPokeballClientRpc(-1);
        }
    }

public void OnButtonBuyPokemon3(){
    int playeriterator = dc.tempplayersarrayvalue.Value;
          if(inv.PlayerInventory[playeriterator].NumberOfPokeballs==0){
            ChangePopTextServerRpc("Brakuje ci pokeballi!");
        }else if(mm.GetMoney(playeriterator) - 50 < 0){
            PlayerPopText.text = "Nie stać cie na kupno!";
            WaitThenClearPopText(1000);
        }else{
            BuyPokemonServerRpc(2);
            inv.PlayerInventory[playeriterator].AddPokeballClientRpc(-1);
        }
    }
    [ServerRpc(RequireOwnership =false)]
    void ChangePopTextServerRpc(string text){
         PlayerPopText.text = text;
            WaitThenClearPopText(1000);
    }


    [ServerRpc(RequireOwnership =false)]

    void BuyPokemonServerRpc(int i){
        mm.ChangePlayerMoneyClientRpc(dc.tempplayersarrayvalue.Value, -50);
        BuyPokemonClientRpc(i);
    }
    [ClientRpc]
    void BuyPokemonClientRpc(int i){
          inv.PlayerInventory[dc.tempplayersarrayvalue.Value].AddPokemon(pa.PokemonOnFields[Tag][i]);
        
        pa.PokemonOnFields[Tag][i].name = "DELETED";
    }


   [ClientRpc]
    void ChangeWindowTitleClientRpc(){
        
        if (pa.FieldType.ContainsKey(Tag )&& !dc.playersThatLost[NetworkManager.Singleton.LocalClientId]){

            titletext.text = pa.FieldType[Tag];
            titletext.color = HexToColor(SetTypeColor(pa.FieldType[Tag]));
            //titletext.text = fieldStatus.FieldsStatutes[Tag];
            if (fieldStatus.FieldsStatutes[Tag].Contains("Bought"))
            {
                HealPokemonsbutton.gameObject.SetActive(false);
                BuyPokeballsbutton.gameObject.SetActive(false);
                ulong playernumber = ulong.Parse(fieldStatus.FieldsStatutes[Tag].Substring(12));
                 playernumber--;
                if (NetworkManager.Singleton.LocalClientId == playernumber)
                {
                    OwnerText.text = "To jest Twoje pole";
                    InfoText.text = "To pole już zostało przez ciebie kupione!";
                    Cancelbutton.gameObject.SetActive(true);
                    Paybutton.gameObject.SetActive(false);
                    Continuebutton.gameObject.SetActive(false);
                    if (pa.PokemonOnFields[Tag][0].name != "DELETED" || pa.PokemonOnFields[Tag][1].name != "DELETED" || pa.PokemonOnFields[Tag][2].name != "DELETED")
                    {
                        OpenPokeMenubutton.gameObject.SetActive(true);
                    }else OpenPokeMenubutton.gameObject.SetActive(false);
                }
                else
                {
                    OwnerText.text = "Kupione przez: Gracz" + ulong.Parse(fieldStatus.FieldsStatutes[Tag].Substring(12))
                    + "\n Płacisz 100€ czynszu";
                    InfoText.text = "";
                    Cancelbutton.gameObject.SetActive(false);
                    Paybutton.gameObject.SetActive(true);
                    Continuebutton.gameObject.SetActive(false);
                    OpenPokeMenubutton.gameObject.SetActive(false);

                }
            }
            else if (fieldStatus.FieldsStatutes[Tag].Contains("Ticket"))
            {
                OwnerText.text = "Zostałeś napadnięty przez zespół R \n Musisz zapłacić 150€";
                InfoText.text = "";
                Cancelbutton.gameObject.SetActive(false);
                Paybutton.gameObject.SetActive(true);
                Continuebutton.gameObject.SetActive(false);
                OpenPokeMenubutton.gameObject.SetActive(false);
                 HealPokemonsbutton.gameObject.SetActive(false);
                BuyPokeballsbutton.gameObject.SetActive(false);
            }

            else if (fieldStatus.FieldsStatutes[Tag].Contains("Default"))
            {
                InfoText.text = "Kupując to pole zapłacisz 150€\ni wylosujesz 3 pokemony danego typu\nkażdy gracz który wejdzie na to pole zapłaci 100€";
                OwnerText.text = "";
                Cancelbutton.gameObject.SetActive(true);
                Paybutton.gameObject.SetActive(false);
                Continuebutton.gameObject.SetActive(false);
                OpenPokeMenubutton.gameObject.SetActive(false);
                 HealPokemonsbutton.gameObject.SetActive(false);
                BuyPokeballsbutton.gameObject.SetActive(false);
            }
            else if (fieldStatus.FieldsStatutes[Tag].Contains("PokeCentre"))
            {
                InfoText.text = "W PokeCentre możesz wyleczyć pokemony\noraz kupic pokeballe\n Koszt leczenia to 80€\nKoszt pokeballa to 40€";
                Cancelbutton.gameObject.SetActive(true);
                Paybutton.gameObject.SetActive(false);
                Continuebutton.gameObject.SetActive(false);
                OwnerText.text = "";
                OpenPokeMenubutton.gameObject.SetActive(false);
                 HealPokemonsbutton.gameObject.SetActive(true);
                BuyPokeballsbutton.gameObject.SetActive(true);
            }
            else if (fieldStatus.FieldsStatutes[Tag].Contains("BlueP"))
            {
                InfoText.text = "Jesteś na polu szansy, wylosuj karte";
                Cancelbutton.gameObject.SetActive(false);
                Paybutton.gameObject.SetActive(false);
                Continuebutton.gameObject.SetActive(true);
                OwnerText.text = "";
                OpenPokeMenubutton.gameObject.SetActive(false);
                 HealPokemonsbutton.gameObject.SetActive(false);
                BuyPokeballsbutton.gameObject.SetActive(false);

            }
            else if (fieldStatus.FieldsStatutes[Tag].Contains("YellowP"))
            {
                InfoText.text = "Jesteś na polu wyzwania, wylosuj karte";
                Cancelbutton.gameObject.SetActive(false);
                Paybutton.gameObject.SetActive(false);
                Continuebutton.gameObject.SetActive(true);
                OwnerText.text = "";
                OpenPokeMenubutton.gameObject.SetActive(false);
                 HealPokemonsbutton.gameObject.SetActive(false);
                BuyPokeballsbutton.gameObject.SetActive(false);

            }
            else if (fieldStatus.FieldsStatutes[Tag].Contains("Escape"))
            {
                InfoText.text = "Wyjście z jaskinii";
                Cancelbutton.gameObject.SetActive(false);
                Paybutton.gameObject.SetActive(false);
                Continuebutton.gameObject.SetActive(true);
                OwnerText.text = "";
                OpenPokeMenubutton.gameObject.SetActive(false);
                 HealPokemonsbutton.gameObject.SetActive(false);
                BuyPokeballsbutton.gameObject.SetActive(false);

            }
            else if (fieldStatus.FieldsStatutes[Tag].Contains("Road"))
            {
                InfoText.text = "Zwykła spokojna droga";
                Cancelbutton.gameObject.SetActive(false);
                Paybutton.gameObject.SetActive(false);
                Continuebutton.gameObject.SetActive(true);
                OwnerText.text = "";
                OpenPokeMenubutton.gameObject.SetActive(false);
                 HealPokemonsbutton.gameObject.SetActive(false);
                BuyPokeballsbutton.gameObject.SetActive(false);
                

            }
            else if (fieldStatus.FieldsStatutes[Tag].Contains("Cave"))
            {
                InfoText.text = "Wchodzisz do jaskini, tracisz dwie tury";
                Cancelbutton.gameObject.SetActive(false);
                Paybutton.gameObject.SetActive(false);
                Continuebutton.gameObject.SetActive(true);
                OwnerText.text = "";
                OpenPokeMenubutton.gameObject.SetActive(false);
                 HealPokemonsbutton.gameObject.SetActive(false);
                BuyPokeballsbutton.gameObject.SetActive(false);

            }
              else if (fieldStatus.FieldsStatutes[Tag].Contains("Start"))
            {
                InfoText.text = "Jesteś na starcie";
                Cancelbutton.gameObject.SetActive(false);
                Continuebutton.gameObject.SetActive(true);
                Paybutton.gameObject.SetActive(false);
                OwnerText.text = "";
                OpenPokeMenubutton.gameObject.SetActive(false);
                 HealPokemonsbutton.gameObject.SetActive(false);
                BuyPokeballsbutton.gameObject.SetActive(false);

            }
             else if (fieldStatus.FieldsStatutes[Tag].Contains("Reward"))
            {
                 HealPokemonsbutton.gameObject.SetActive(false);
                BuyPokeballsbutton.gameObject.SetActive(false);
                OpenPokeMenubutton.gameObject.SetActive(false);
                if (reward == -1 && !dc.ShouldBeCanvasShown.Value)
                {
                     reward = Random.Range(1, 10) * 100;
                    InfoText.text = "Gratulacje! Wylosowałeś " + reward + "€ nagrody";
                    
                    Cancelbutton.gameObject.SetActive(false);
                    Continuebutton.gameObject.SetActive(true);
                    OwnerText.text = "";
                }
            }
            else
            {
                OwnerText.text = "";
                Cancelbutton.gameObject.SetActive(true);
                 Paybutton.gameObject.SetActive(false);
                  HealPokemonsbutton.gameObject.SetActive(false);
                BuyPokeballsbutton.gameObject.SetActive(false);

            }

        }
        
    }
   

 
    void ShowCanvasAfterDelay()
    {
         dc.CanNextPlayerMoveServerRpc(false);
        canvas.SetActive(true);
        dc.DisableCanvasServerRpc();
        if(fieldStatus.FieldsStatutes[Tag]!="Default"){
            Buybutton.gameObject.SetActive(false);
        }else{
            Buybutton.gameObject.SetActive(true);
        }
        
    }
    [ServerRpc(RequireOwnership =false)]
    void SetupCanvasServerRpc(string str, int randomNumber){
        cc.SetupCanvasClientRpc(str,randomNumber);
    }
    public void OnButtonEndInteraction(){
        PanelInteractWithField.SetActive(true);
        PanelToBuyPokemons.SetActive(false);
        canvas.SetActive(false);
        Continuebutton.gameObject.SetActive(false);
        if(InfoText.text.Contains("Gratulacje! Wylosowałeś")){
            mm.ChangePlayerMoneyClientRpc(dc.TourIterator.Value, reward);
        }
        if (fieldStatus.FieldsStatutes[Tag].Contains("BlueP"))
        {
            int randomNumber = Random.Range(0,13);
            //int randomNumber = 5;
            SetupCanvasServerRpc("Chance",randomNumber);
        }
        else if (fieldStatus.FieldsStatutes[Tag].Contains("YellowP"))
        {
            int randomNumber = Random.Range(0,12);//x
             //int randomNumber = 10;
            SetupCanvasServerRpc("Challenge",randomNumber);
        }else{
            
        dc.DisableCanvasServerRpc();
         
        dc.CanNextPlayerMoveServerRpc(true);
        }




         if(fieldStatus.FieldsStatutes[Tag].Contains("Cave")){

            PutPlayerInCaveServerRpc();
         }
        if(fieldStatus.FieldsStatutes[Tag].Contains("Bought"))mm.PayIfNeeded(-100,"Bought",fieldStatus,Tag);
        if(fieldStatus.FieldsStatutes[Tag].Contains("Ticket"))mm.PayIfNeeded(-150,"Ticket",fieldStatus,Tag);
      reward = -1;
      
            
        
    }
    [ServerRpc(RequireOwnership =false)]
    void PutPlayerInCaveServerRpc(){
dc.IsInWaiting[dc.tempplayersarrayvalue.Value] = 2;
    }
    public void OnButtonBuyField(){

        PanelInteractWithField.SetActive(false);
        PanelToBuyPokemons.SetActive(true);
        ChangeFieldStatusServerRpc();
        
    }

    
    public void OnButtonBuyPokeBalls(){
              int playeriterator = dc.tempplayersarrayvalue.Value;
        if (mm.GetMoney(playeriterator) - 40 < 0)
        {

            PlayerPopText.text = "Nie stać cie na kupno!";
            WaitThenClearPopText(1000);

        }else if( inv.PlayerInventory[(int)NetworkManager.Singleton.LocalClientId].NumberOfPokeballs+
         inv.PlayerInventory[(int)NetworkManager.Singleton.LocalClientId].PokemonList.Count == 8){
            PlayerPopText.text = "Kupiłeś już 8 pokeballi!";
            WaitThenClearPopText(1000);
        }
        else
        {
            mm.ChangePlayerMoneyServerRpc(NetworkManager.Singleton.LocalClientId, -40);
            inv.PlayerInventory[(int)NetworkManager.Singleton.LocalClientId].AddPokeballClientRpc(1);
        }
    }
    [ServerRpc(RequireOwnership =false)]
    public void OnButtonHealPokemonsServerRpc() {
        
         OnButtonHealPokemonsClientRpc();
        
    }
    [ClientRpc]
    public void OnButtonHealPokemonsClientRpc() {
        int playeriterator = dc.tempplayersarrayvalue.Value;
         bool AllFullHP = inv.PlayerInventory[dc.tempplayersarrayvalue.Value].PokemonList.All(element => element.HP == element.Base.HP);
         if(inv.PlayerInventory[dc.tempplayersarrayvalue.Value].PokemonList.Count==0 && playeriterator==(int)NetworkManager.Singleton.LocalClientId){
            PlayerPopText.text = "Nie masz pokemonów!";
            WaitThenClearPopText(1000);
         }
         else if(AllFullHP && playeriterator==(int)NetworkManager.Singleton.LocalClientId){
            PlayerPopText.text = "Wszystkie pokemony są zdrowe!";
            WaitThenClearPopText(1000);
         }
        else if (mm.GetMoney(playeriterator) - 80 < 0 && playeriterator==(int)NetworkManager.Singleton.LocalClientId ) {

            PlayerPopText.text = "Nie stać cie na kupno!";
            WaitThenClearPopText(1000);

        } else if(mm.GetMoney(playeriterator) - 80 >= 0 && inv.PlayerInventory[dc.tempplayersarrayvalue.Value].PokemonList.Count!=0
        && !AllFullHP){
            foreach (var pokemon in inv.PlayerInventory[dc.tempplayersarrayvalue.Value].PokemonList)
            {
                
                pokemon.HP = pokemon.Base.HP;
            }
        
            mm.ChangePlayerMoneyClientRpc(dc.tempplayersarrayvalue.Value, -80);
        } 
        }
    [ServerRpc(RequireOwnership = false)]
    void ChangeFieldStatusServerRpc(){
        int playeriterator = dc.tempplayersarrayvalue.Value;
        if(mm.GetMoney(playeriterator)-150<0){
            
            PlayerPopText.text = "Nie stać cie na kupno!";
            WaitThenClearPopText(1000);
            
        }
        else
        {
            fieldStatus.FieldsStatutes[Tag] = "Bought" + dc.Players[playeriterator].name;
            ChangeFieldStatusClientRpc(fieldStatus.FieldsStatutes[Tag], playeriterator);
            
            CreateCubeForFieldClientRpc(playeriterator);
            
            
        }
      
    }
    async void WaitThenClearPopText(int time){
        await Task.Delay(time);
        PlayerPopText.text = "";
    }
    [ClientRpc]
    void ChangeFieldStatusClientRpc(string status, int playeriterator){
        fieldStatus.FieldsStatutes[Tag] = status;
        mm.ChangePlayerMoneyClientRpc(playeriterator, -150);
        
    }
    
  
  
   [ClientRpc]
   void CreateCubeForFieldClientRpc(int playeriterator){
      GameObject FieldBoughtCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        GameObject tempGM = GameObject.Find(Tag);
        FieldBoughtCube.transform.position = new Vector3(tempGM.transform.position.x,tempGM.transform.position.y+0.1f,tempGM.transform.position.z);
        FieldBoughtCube.transform.localScale = new Vector3(0.5f, 0.1f, 1f);
        FieldBoughtCube.name = dc.Players[playeriterator] + Tag;
        FieldBoughtCube.GetComponent<MeshRenderer>().shadowCastingMode = ShadowCastingMode.Off;
       
       
       FieldBoughtCube.GetComponent<Renderer>().material = dc.Players[playeriterator].GetComponent<Renderer>().material;
   }
     public string SetTypeColor(string type){
        switch(type){
            case "Normal/Dragon":
                type = "#ffa500";
                break;
           
            case "Water/Ice":
                type = "#0099CC";
                break;  
        
            case "Grass":
                type = "#00CC33";
                break;
            case "Bug":
                type = "#339933";
                break;
            case "Fire":
                type = "#FF3333";
                break;
            case "Rock":
                type = "#AAA";
                break;
            case "Ghost":
                type = "#000";
                break;
            case "Electro":
                type = "#FFFF00";
                break;
            case "Poison":
                type = "#990099";
                break;
            case "Psycho":
                type = "#CC3399";
                break;
            case "Ground":
                type = "#996633";
                break;
            case "Fighting":
                type = "#C0C0C0";
                break;
            default:
                break;

        }
        return type;
    }
       Color HexToColor(string hex)
    {
        Color color = Color.white;
        ColorUtility.TryParseHtmlString(hex, out color);
        return color;
    }
   

}