using System.Collections;
using UnityEngine;
using Unity.Netcode;
using Unity.VisualScripting.FullSerializer;
using Unity.VisualScripting;
using System.Linq;
using System.Threading.Tasks;
using System;
using TMPro;
using Random = UnityEngine.Random;
using UnityEditor;
using UnityEngine.UI;

public class Dice : NetworkBehaviour
{
    int playercounter = 0;
    public GameObject[] Players; // Tablica obiektow graczy
    public Material[] Materials; // Tablica materialow, zeby kazdy gracz mial inny kolor
    public int PlayersArrayLength = 0; // Ilosc polaczonych osob na serwerze
    public GameObject PlayerTourActive; // Obiekt gracza ktory aktualnie ma ture
    public NetworkVariable<byte> TourIterator = new(0); // Iteracja tur od 1(jeden gracz) do max4(zalezy od ilosc graczy)
    public DiceMovement dm; // Komponent DiceMovement, potrzebny do : uruchamianie animacji kostki
    public DiceMovement dm2;
    public GameObject Dice2;
    public PlayerMovementNT pm; // Komponent PlayerMovementNT, potrzebny do : ruszanie pionkami graczy
    public int DiceNumber; // Wylosowana liczba na kostce
    public string Tag = ""; // Tag gracza, sluzy do przekazania informacji komponentowi PlayerMovementNT na jakie pole ma isc gracz
    public NetworkVariable<int> TagNumber = new(1);
    public GameObject NullGM; // Sluzy do przypisania pustych miejsc tablicy Players, aby odroznic braku gracza od jego opuszczenia(null)
    internal int FieldNumber = 40; // Ilosc pol na calej planszy
    public ulong localID; // Lokalne ID danego klienta
    public NetworkVariable<float> timer = new(-3); // Timer sluzy do : odczekania po ostatnim kliknieciu przycisku
    public Animator animator; // Komponent animator sluzy do odczekania, sprawdzam przy jego pomocy w jakim stanie jest animacja
     public FieldCanvas fc;
    public MoneyManager mm;
     [SerializeField] GameObject FieldCanvasConnect;
     [SerializeField] GameObject Canvas;
    public NetworkVariable<bool>  ShouldBeCanvasShown = new(false);
    public NetworkVariable<bool>  CanNextPlayerMove = new(true);
    public NetworkVariable<int> tempplayersarrayvalue;
    public bool[] playersThatLost;
    public int[] IsInWaiting;
    [SerializeField] TextMeshProUGUI TourText;
    [SerializeField] GameObject InventoryObject;
    PlayersInventories pi;
    public Button ThrowDice;
    void Start()
    {
        timer.Value = -7;
        
        IsInWaiting = new int[4];
        playersThatLost = new bool[4];
        for (int i = 0; i <4; i++)
        {
            IsInWaiting[i] = 0;
            playersThatLost[i] = false;
        }


        CanNextPlayerMove.Value = true;
        pi = InventoryObject.GetComponent<PlayersInventories>();
        mm = Canvas.GetComponent<MoneyManager>();
        fc = FieldCanvasConnect.GetComponent<FieldCanvas>();
        TourIterator.Value = 0; // Start, zaczyna gracz 0

        for (int i = 0; i < Players.Length; i++) // Przypisanie wszystkim polom NullGM, opisane przy zmiennej czemu.
        {
            Players[i] = NullGM;
        }
    }

    void Update()
    {
       
        if(IsHost || IsServer)CheckIfAllPlayersAreInCaveServerRpc();
        if (CanNextPlayerMove.Value && timer.Value <= -6)
        {
            if (Players[TourIterator.Value] != NullGM)
            {
                MeshRenderer playerrenderer = Players[TourIterator.Value].GetComponent< MeshRenderer>();
                TourText.color = playerrenderer.material.color;
                TourText.text = "Obecnie rusza : Gracz " + (TourIterator.Value + 1);
            }
        }
        

        for (int i = 0; i < PlayersArrayLength; i++)
        {

            if (!playersThatLost[i]) break;
            if (i == PlayersArrayLength - 1) Application.Quit();

            
        }
        
        localID = NetworkManager.Singleton.LocalClientId; // Przypisanie lokalnego ID, kazdy klient ma inne
        if (TourIterator.Value == localID && CanNextPlayerMove.Value&&timer.Value<=-5) ThrowDice.gameObject.SetActive(true);
        else ThrowDice.gameObject.SetActive(false);

      
        if(ShouldBeCanvasShown.Value)fc.ShowCanvas((byte)tempplayersarrayvalue.Value,localID);
         

        for (int i = PlayersArrayLength; i < Players.Length; i++) // Przypisanie kazdemu graczowi tag P1, pole startowe
        {
            if (Players[i] != NullGM)
            {
                PlayersArrayLength++;
                Players[i].tag = "P1";
            }
        }

        for (int i = 0; i < Players.Length; i++) //W momencie opuszczenia przez kogos serwera, element tablicy Players zostaje zastapiany 'null'
                                                // wiec jezeli program to wykryje zastepuje go NullGM i  PlayersArrayLength--
        {
            if (Players[i] == null)
            {
                Players[i] = NullGM;
                //PlayersArrayLength--;
            }
        }

        if (IsServer || IsHost) timer.Value -= Time.deltaTime; // Liczenie czasu, tylko na serwerze
        SetPlayersToArray(); 
    }



    [ServerRpc]
   void CheckIfAllPlayersAreInCaveServerRpc(){
    int temp = 0;
        if (playercounter > 0)
        {
            for (int i = 0; i < playercounter; i++)
            {

               
                if (IsInWaiting[i] != 0) temp++;
                
            }
            if (temp == playercounter)
            {
                
                
                while (playersThatLost[TourIterator.Value] || IsInWaiting[TourIterator.Value] != 0)
                {
                    if(!playersThatLost[TourIterator.Value])IsInWaiting[TourIterator.Value]--;
                    if (IsInWaiting[TourIterator.Value] == 0)
                    {

                        GameObject escpobj = GameObject.Find("P11");
                        Players[TourIterator.Value].transform.position = new Vector3(Players[TourIterator.Value].transform.position.x,
                         Players[TourIterator.Value].transform.position.y + 3f, Players[TourIterator.Value].transform.position.z);
                        StartCoroutine(WaitForPlayerToDSP(TourIterator.Value, escpobj));
                        Players[TourIterator.Value].tag = "P11";
                        
                    }
                    
                    
                        TourIterator.Value = (byte)((TourIterator.Value + 1) % PlayersArrayLength);
                    
                }
            }


        }
    }
/// <summary>
/// W skrocie, nadanie materialow prefabom, ustawienie pionkow na pozycji startowej, Powinno byc w innym skrypcie!!
/// </summary>
    void SetPlayersToArray()
    {
        GameObject objtoadd = GameObject.Find("Player" + 1 + "(Clone)");
        
        for (int i = 0; i < Players.Length; i++)
        {
            if (Players[i] == NullGM && objtoadd is not null)
            {
                playercounter++;
                MeshRenderer objtoaddCom = objtoadd.GetComponent<MeshRenderer>();
                objtoaddCom.material = Materials[i];
                objtoadd.name = "Player" + (i + 1);
                pi.AddPlayer(i);

                objtoadd.transform.rotation = Quaternion.Euler(-90, 0, 0);
                var pos = objtoadd.transform.position;

                switch (i)
                {
                    case 0:
                        objtoadd.transform.position = new Vector3(pos.x + 0.1f, pos.y, pos.z + 4f);
                        break;
                    case 1:
                        objtoadd.transform.position = new Vector3(pos.x + 0.7f, pos.y, pos.z + 4.6f);
                        break;
                    case 2:
                        objtoadd.transform.position = new Vector3(pos.x + 0.7f, pos.y, pos.z + 4f);
                        break;
                    case 3:
                        objtoadd.transform.position = new Vector3(pos.x + 0.1f, pos.y, pos.z + 4.6f);
                        break;
                }

                Players[i] = objtoadd;
                objtoadd = null;
            }
        }
    }
  /// <summary>
  /// Funkcja wywolywana po kliknieciu przycisku do rzutu kostki, wywoluje ona druga funkcje poniewaz [ServerRpc] moze wywolac tylko serwer
  /// ale chce na kliencie sprawdzic czy jego ID pasuje do odpowiedniej tury. np dla gracza 0 musi byc tura 0 zeby mogl ruszyc
  /// </summary>
    public void CallDiceButtonClick() 
    {
  
        if (localID == TourIterator.Value)
        {
            DiceButtonClickServerRpc();
        }
        else
        {
            Debug.Log("TWOJE ID : " + localID);
            Debug.Log("RUCH POWINIEN BYĆ: " + TourIterator.Value);
        }
    }
/// <summary>
/// timer powoduje cooldown X sekundowy.
/// Wylosowanie liczby, wywolanie funkcji AnimDice aby wykonac animacje kostki.
/// Po wylosowaniu dopiero ruszyc pionkiem na dane pole, TourIterator++ aby nastepny gracz mogl ruszyc.
/// Kazdy gracz ma Tag informujacy na jakim polu sie znajduje. np P15 jezeli wylosuje 5, przeniesie gracza na P20
/// </summary>
    [ServerRpc(RequireOwnership = false)]
    public void DiceButtonClickServerRpc()
    {
        if (CanNextPlayerMove.Value&&timer.Value<=0 && Players[0] != NullGM)
        {
            timer.Value = 2f;
            dm = GetComponent<DiceMovement>();
            dm2 = Dice2.GetComponent<DiceMovement>();
            pm = GetComponent<PlayerMovementNT>();
      animator = GetComponent<Animator>();
            PlayerTourActive = Players[TourIterator.Value];
             int diceNumber = Random.Range(1,7);
             int diceNumber2 = Random.Range(1,7);
           // int diceNumber = 4;
            //int diceNumber2 = 4;
            int eq = int.Parse(PlayerTourActive.tag.Substring(1)) + diceNumber + diceNumber2;
        

            if(eq > FieldNumber){
                Tag = "P" + (eq - FieldNumber);
                
                
            }else{
                Tag= "P" + eq;
            }
            
            TagNumber.Value = int.Parse(Tag.Substring(1));
            //fc.Tag = Tag;
            // fc.TakeTag(TagNumber.Value); // Przekazanie Tag do FieldCanvas
            fc.TagNumber.Value = int.Parse(Tag.Substring(1));
            dm.AnimDice(diceNumber);
            dm2.AnimDice(diceNumber2);

            StartCoroutine(WaitForCube());


            tempplayersarrayvalue.Value = TourIterator.Value;
            TourIterator.Value = (byte)((TourIterator.Value + 1) % PlayersArrayLength);
            int temp = 0;

     for (int i = 0; i < playercounter; i++)
            {
                if (IsInWaiting[i] != 0) temp++;
            }

           
            if (Tag!="P31" || temp!=playercounter-1)
            {
               
                
                while (playersThatLost[TourIterator.Value] || IsInWaiting[TourIterator.Value] != 0)
                {
                   if(!playersThatLost[TourIterator.Value])IsInWaiting[TourIterator.Value]--;
                    int i = TourIterator.Value;
                    if (IsInWaiting[i] == 0)
                        {

                            GameObject escpobj = GameObject.Find("P11");



                            Players[i].transform.position = new Vector3(Players[i].transform.position.x,
                             Players[i].transform.position.y + 3f, Players[i].transform.position.z);
                            StartCoroutine(WaitForPlayerToDSP(i, escpobj));
                            Players[i].tag = "P11";
                        }
                    TourIterator.Value = (byte)((TourIterator.Value + 1) % PlayersArrayLength);

                }
              
            }else{

            }
                 
        }
    }


  /// <summary>
  /// Sluzy do czekania
  /// </summary>
  /// <returns></returns>
  /// 
    IEnumerator WaitForCube()
    {
        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);
        
        pm.MoveTo(Tag, PlayerTourActive, PlayersArrayLength);
        PlayerTourActive.tag = Tag;
        ShouldBeCanvasShown.Value = true;       
    }
    IEnumerator WaitForPlayerToDSP(int i, GameObject escpobj)
    {
        yield return new WaitForSecondsRealtime(1.6f);
        
        Players[i].transform.position = new Vector3(escpobj.transform.position.x,
         Players[i].transform.position.y, escpobj.transform.position.z);
         yield return new WaitForSecondsRealtime(1.6f);
        
          Players[i].transform.position = new Vector3(Players[i].transform.position.x,
           Players[i].transform.position.y-3f, Players[i].transform.position.z);

    }
    [ServerRpc(RequireOwnership = false)]
    public void DisableCanvasServerRpc(){
        ShouldBeCanvasShown.Value = false;
        timer.Value = -10;
    }
     [ServerRpc(RequireOwnership =false)]
    public void CanNextPlayerMoveServerRpc(bool can){
        CanNextPlayerMove.Value = can;
    }
    public void GetLostPlayer(int id){
        playersThatLost[id] = true;
    }

}
