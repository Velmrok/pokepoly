using System.Net;
using System.Net.Sockets;
using TMPro;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using UnityEngine;
using UnityEngine.UI;



public class MainMenu : NetworkBehaviour
{
    [SerializeField] GameObject MenuCanvas;
    [SerializeField] GameObject eqbutton;
    [SerializeField] string ipAddress;
    [SerializeField] Button[] ButtonsFirstWindow;
    [SerializeField] Button[] ButtonsSecondWindow;
    [SerializeField] TMP_InputField ip;
    [SerializeField] UnityTransport transport;
    [SerializeField] Slider progressBar;
    byte setactivejustone = 0;

    public void Start(){
        eqbutton.gameObject.SetActive(false);
        ip.gameObject.SetActive(false);
         foreach (var button in ButtonsSecondWindow)
        {
             button.gameObject.SetActive(false);
        }
    }
    public void Update(){
        
        if (progressBar.value!=1 && setactivejustone==0){
            foreach(var button in ButtonsFirstWindow){
                button.gameObject.SetActive(false);
            }
        }
        else if(setactivejustone==0){
            foreach(var button in ButtonsFirstWindow){
                setactivejustone = 1;
                button.gameObject.SetActive(true);
                
            }
        }
    }
    public void OnButtonCreateGame()
    {
        NetworkManager.Singleton.StartHost();
        GetLocalIPAddress();
        MenuCanvas.SetActive(false);
        eqbutton.gameObject.SetActive(true);
    }
    public void StartClient() {
		
		
        //ipAddress = ip.text;
		//SetIpAddress();
        NetworkManager.Singleton.StartClient();
        MenuCanvas.SetActive(false);
         eqbutton.gameObject.SetActive(true);
	}
    public void OnButtonGoBack(){
         foreach (var button in ButtonsFirstWindow)
        {
            button.gameObject.SetActive(true);
        }
        foreach (var button in ButtonsSecondWindow)
        {
             button.gameObject.SetActive(false);
        }
        ip.gameObject.SetActive(false);
    }


    public void OnButtonJoinGame(){
        foreach (var button in ButtonsFirstWindow)
        {
            button.gameObject.SetActive(false);
        }
        foreach (var button in ButtonsSecondWindow)
        {
             button.gameObject.SetActive(true);
        }
        ip.gameObject.SetActive(true);
        
    }
    public void OnButtonExit(){
        Application.Quit();
    }


    public string GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork )
            {
                
                ipAddress = ip.ToString();
                return ip.ToString();
            }
        }
        throw new System.Exception("No network adapters with an IPv4 address in the system!");
    }
    public void SetIpAddress() {
		transport = NetworkManager.Singleton.GetComponent<UnityTransport>();
		transport.ConnectionData.Address = ipAddress;
	}
    

}