using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class ScreenManager : MonoBehaviour
{
    [SerializeField] TMP_Dropdown resulutiondropdown;
    private Resolution[] resolutions;
    private List<Resolution> filteredResolutions;
    private double currentRefreshRate;
    private int currentresindex;
    void Start(){
        Screen.fullScreen = true;
        resolutions = Screen.resolutions;
        filteredResolutions = new List<Resolution>();
        resulutiondropdown.ClearOptions();
        currentRefreshRate = Screen.currentResolution.refreshRateRatio.value;
        foreach (var res in resolutions)
        {
            if(res.refreshRateRatio.value==currentRefreshRate){
                filteredResolutions.Add(res);
            }
        }

        List<string> options = new List<string>();
        foreach(var res in filteredResolutions){
            string resopt = res.width + "x" + res.height + " " + res.refreshRateRatio.value + " Hz";
            options.Add(resopt);
            if(res.width==Screen.currentResolution.width && res.height==Screen.currentResolution.height)
            currentresindex = filteredResolutions.IndexOf(res);
        }

        resulutiondropdown.AddOptions(options);
        resulutiondropdown.value = currentresindex;
        resulutiondropdown.RefreshShownValue();

    }
    public void SetResolution(int index){
        Resolution res = filteredResolutions[index];
        Screen.SetResolution(res.width, res.height, Screen.fullScreen);

    }
    public void ChangeFullScreenMode(){
        Screen.fullScreen = !Screen.fullScreen;
    }
}
