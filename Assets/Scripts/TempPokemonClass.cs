using System.Collections.Generic;
using Newtonsoft.Json;
[System.Serializable]
public class TempPokemon
{

    public int id;

    [JsonProperty("name")]
    public Dictionary<string, string> name;


    public string[] type;


    public PokemonBaseStats Base;
}

[System.Serializable]
public class PokemonBaseStats
{
    
    public int HP;

   
    public int Attack;

    
    public int Defense;

    // Możesz pominąć pozostałe właściwości, których nie chcesz używać
}
[System.Serializable]
public class TempPokemonListContainer
{
    public List<TempPokemon> PokemonList;
}
