using Unity.Netcode;
using Unity.Netcode.Components;

public class DiceMovement : NetworkBehaviour
{   
    private NetworkAnimator networkAnimator; // pobiera networkanimator, zeby animowac na kazdym kliencie

    private void Start()
    {
        networkAnimator = GetComponent<NetworkAnimator>();
    }
    public void AnimDice(int DiceNumber)
    {
        RpcOdtworzAnimacjeClientRpc("Trigger" + DiceNumber);      
    }
    [ClientRpc]
    void RpcOdtworzAnimacjeClientRpc(string anim)
    {
        // Odtwórz animację na wszystkich klientach
        networkAnimator.SetTrigger(anim);
    }
}
