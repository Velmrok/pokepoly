using System.Collections.Generic;
using System.IO;
using Unity.Netcode;
using UnityEngine;
using Newtonsoft.Json;
using TMPro;
using System.Collections;
using UnityEngine.UI;
using Unity.VisualScripting;
using Unity.Mathematics;
using System;
using Random = UnityEngine.Random;

public class ChanceChallenge: NetworkBehaviour
{
    string[] ChanceArray;
    string[] ChallengeArray;

    public int ChanceCount;
    public int ChallengeCount;
    [SerializeField] GameObject Canvas;
    [SerializeField] GameObject MainCanvas;
    [SerializeField] TextMeshProUGUI info;
    [SerializeField] GameObject OKButton;
    [SerializeField] Image CardImage;
    [SerializeField] Sprite[] CardSprites;
    [SerializeField] GameObject InventoryObject;
    PlayersInventories pi;
    int number;
    int tempnumber;
    bool isitchance;

    MoneyManager mm;
    Dice dice;
    PlayerMovementNT pm;


    void Start(){
        pi = InventoryObject.GetComponent<PlayersInventories>();
        mm = MainCanvas.GetComponent<MoneyManager>();
        dice = GetComponent<Dice>();
        pm = GetComponent<PlayerMovementNT>();

        string json = File.ReadAllText(Application.streamingAssetsPath+"/DateBase/DateBase/Chance.json");
     
        ChanceArray = JsonConvert.DeserializeObject<string[]>(json);

        string json2 = File.ReadAllText(Application.streamingAssetsPath+"/DateBase/DateBase/Challenge.json");
     
        ChallengeArray = JsonConvert.DeserializeObject<string[]>(json2);

        ChanceCount = ChanceArray.Length;
        ChallengeCount = ChallengeArray.Length;
        Canvas.SetActive(false);
   
    }
      [ServerRpc(RequireOwnership =false)]
    void SetupCanvasServerRpc(string str, int randomNumber){
        SetupCanvasClientRpc(str,randomNumber);
    }
    [ClientRpc]
    public void SetupCanvasClientRpc(string str, int number){
        this.number = number;
        if(str=="Chance"){
            CardImage.sprite = CardSprites[0];
            
            info.text = ChanceArray[number];
            isitchance = true;
            //CardSprites[0];
        }
        else{
            CardImage.sprite = CardSprites[1];
            isitchance = false;
            info.text = ChallengeArray[number];
        }
        if(dice.tempplayersarrayvalue.Value==(int)NetworkManager.Singleton.LocalClientId){
            OKButton.SetActive(true);
        }else{
            OKButton.SetActive(false);
        }
    Canvas.SetActive(true);
    }

    public void OnButtonAccept(){
        if (isitchance) ExecuteChance();
        else ExecuteChallenge();
        
    }
    public void OnButtonAcceptChallenge()
    {

    }

    public void ExecuteChance()
    {

        var Player = dice.Players[dice.tempplayersarrayvalue.Value];
        switch (number)
        {
            case 0:
                MoveToServerRpc(6);


                break;

            case 1:
                mm.ChangePlayerMoneyServerRpc((ulong)int.Parse(Player.name.Substring(6)) - 1, 400);
                break;

            case 2:
                MoveToServerRpc(7);

                break;

            case 3:
                mm.ChangePlayerMoneyServerRpc((ulong)int.Parse(Player.name.Substring(6)) - 1, 200);
                break;

            case 4:
                mm.ChangePlayerMoneyServerRpc((ulong)int.Parse(Player.name.Substring(6)) - 1, -200);
                break;

            case 5:
                tempnumber = number;
                SetupCanvasServerRpc("Challenge", 10/*Random.Range(0,12)*/);
                break;

            case 6:
                ChangepokeHpServerRpc(3, 4);

                break;

            case 7:
                ThrowDiceAgainServerRpc();
                if (NetworkManager.Singleton.LocalClientId == (ulong)dice.tempplayersarrayvalue.Value) dice.ThrowDice.gameObject.SetActive(true);

                break;

            case 8:
                mm.ChangePlayerMoneyServerRpc((ulong)int.Parse(Player.name.Substring(6)) - 1, -300);
                break;

            case 9:
                mm.ChangePlayerMoneyServerRpc((ulong)int.Parse(Player.name.Substring(6)) - 1, 300);
                break;

            case 10:
                mm.ChangePlayerMoneyServerRpc((ulong)int.Parse(Player.name.Substring(6)) - 1, -200);
                break;
            case 11:
                MoveToServerRpc(specificTAG: "P31");
               

                break;

            default:
                break;
        }
            if(tempnumber!=5)SetCanvasActiveServerRpc(false);
        if (number != 0 && number != 2 && number != 11 && number != 7 && tempnumber!=5)
        {
            
            dice.DisableCanvasServerRpc();

            dice.CanNextPlayerMoveServerRpc(true);
        }
        tempnumber = 0;
    }
      
        [ServerRpc(RequireOwnership = false)]
        public void ThrowDiceAgainServerRpc()
        {
            dice.TourIterator.Value = (byte)dice.tempplayersarrayvalue.Value;
            dice.CanNextPlayerMove.Value = true;
        }
        [ServerRpc(RequireOwnership = false)]
        void ChangepokeHpServerRpc(int value1, int value2)
        {
            ChangepokeHpClientRpc(value1, value2);
        }
        [ClientRpc]
        void ChangepokeHpClientRpc(int value1, int value2){
            var Player = dice.Players[dice.tempplayersarrayvalue.Value];
                var id = int.Parse(Player.name.Substring(6)) - 1;
                
                if (pi.PlayerInventory[id].PokemonList.Count != 0)
                {
                    Pokemon pokemax = pi.PlayerInventory[id].PokemonList[0];
                    foreach (var pokemon in pi.PlayerInventory[id].PokemonList)
                    {
                        if (pokemon.HP > pokemax.HP) pokemax = pokemon;   
                        else if(pokemon.HP ==pokemax.HP) {
                                if(pokemon.Defense>pokemax.Defense)pokemax = pokemon;
                        }
                    }
                
                pokemax.HP = (float)pokemax.HP * value1/value2%1!=0?
                (int)((float)pokemax.HP * value1/value2 - (float)pokemax.HP * value1/value2%1 +1):pokemax.HP * value1/value2;
                }
        }

[ServerRpc(RequireOwnership =false)]
        void ChangepokemonsHpServerRpc(int value){
        ChangepokemonsHpClientRpc(value);
        }

        [ClientRpc]
        void ChangepokemonsHpClientRpc(int value){
            var Player = dice.Players[dice.tempplayersarrayvalue.Value];
                var id = int.Parse(Player.name.Substring(6)) - 1;
            foreach (var pokemon in pi.PlayerInventory[id].PokemonList)
            {
            pokemon.Base.HP += value;
            pokemon.HP += value;
            }
        }
    
    
    [ServerRpc(RequireOwnership =false)]
    void SetCanvasActiveServerRpc(bool value) {
        SetCanvasActiveClientRpc(value);
     }

    [ClientRpc]
    void SetCanvasActiveClientRpc(bool value){
Canvas.SetActive(value);
    }


    [ServerRpc(RequireOwnership =false)]
    void MoveToServerRpc(int number = 0, string specificTAG = ""){
        string Tag = dice.Tag;
        if(specificTAG!="")Tag = specificTAG;
        else Tag = int.Parse(Tag.Substring(1)) + number > 40 ? "P" + (int.Parse(Tag.Substring(1)) - 32) : "P" + (int.Parse(Tag.Substring(1)) + number);

        pm.MoveTo(Tag, dice.PlayerTourActive, dice.PlayersArrayLength);
        dice.PlayerTourActive.tag = Tag;
        dice.fc.TagNumber.Value = int.Parse(Tag.Substring(1));
        StartCoroutine(Wait(1.7f));
        dice.ShouldBeCanvasShown.Value = true;  
    }
     IEnumerator Wait(float time)
    {
        yield return new WaitForSeconds(time);
     
             
    }


    public void ExecuteChallenge(){
        var Player = dice.Players[dice.tempplayersarrayvalue.Value];
       switch (number)
{
    case 0:
    mm.ChangePlayerMoneyServerRpc((ulong)int.Parse(Player.name.Substring(6)) - 1, 150);
        break;

    case 1:
                ChangepokemonsHpServerRpc(10);
        break;

    case 2:
     mm.ChangePlayerMoneyServerRpc((ulong)int.Parse(Player.name.Substring(6)) - 1, -400);
        break;

    case 3:
                MoveToServerRpc(specificTAG: "P31");
                
        
        break;

    case 4:
        MoveToServerRpc(-5);
        break;

    case 5:
     mm.ChangePlayerMoneyServerRpc((ulong)int.Parse(Player.name.Substring(6)) - 1, -300);
        break;

    case 6:
     mm.ChangePlayerMoneyServerRpc((ulong)int.Parse(Player.name.Substring(6)) - 1, 300);
        break;

    case 7:
                ThrowDiceAgainServerRpc();
        break;

    case 8:
    MoveToServerRpc(specificTAG:"P1");
        break;

    case 9:
    MoveToServerRpc(8);
        break;

    case 10:
    MoveToServerRpc(specificTAG:"P24");
        break;

    case 11:
     mm.ChangePlayerMoneyServerRpc((ulong)int.Parse(Player.name.Substring(6)) - 1, 350);
        break;

    default:
        break;
}
      SetCanvasActiveServerRpc(false);
        if (number!=3&&number!=4&&number!=8&&number!=9&&number!=10)
        {
            
            dice.DisableCanvasServerRpc();

            dice.CanNextPlayerMoveServerRpc(true);
        }

    }


}