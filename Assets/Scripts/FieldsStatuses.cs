using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Unity.Netcode;
using UnityEngine;

public class FieldStatus : NetworkBehaviour
{
  public Dictionary<string, string> FieldsStatutes;
  void Start()
  {
    FieldsStatutes = new Dictionary<string, string>();
    for (int i = 1; i < 41; i++)
    {


      if (i == 1)
      {
        FieldsStatutes.Add("P" + i, "Start");
      }
      else if (i == 4)
      {
        FieldsStatutes.Add("P" + i, "Ticket");
      }
      else if (i == 6 || i == 16 || i == 26 || i == 36)
      {
        FieldsStatutes.Add("P" + i, "PokeCentre");
      }
      else if (i == 9 || i == 29)
      {
        FieldsStatutes.Add("P" + i, "BlueP");
      }
      else if (i == 14 || i == 34)
      {
        FieldsStatutes.Add("P" + i, "YellowP");
      }else if(i==11){
        FieldsStatutes.Add("P" + i, "Escape");
      }else if(i==21){
        FieldsStatutes.Add("P" + i, "Road");
      }
      else if(i==31){
        FieldsStatutes.Add("P" + i, "Cave");
      }
      else if(i==24){
        FieldsStatutes.Add("P" + i, "Reward");
      }
      else
      {
        FieldsStatutes.Add("P" + i, "Default");
      }
    }

  }



  public void ClearAllLostPlayerStatuses(int id){
    var result = FieldsStatutes.Where(kv => kv.Value.Contains("Player"+(id+1))).ToList();
    foreach(var item in result){
      FieldsStatutes[item.Key] = "Default";
    }
  }
}