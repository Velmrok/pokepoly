using System.Collections.Generic;
using System.Diagnostics;

[System.Serializable]
public class Pokemon
{
    public int id;
    public string name;
    public string[] type;
    public Base Base;
    public int HP { get; set; }
    public int Attack { get; set; }
    public int Defense { get; set; }
    
    

  
    
}

[System.Serializable]
public class PokemonListContainer
{
    public List<Pokemon> PokemonList;

    public Pokemon IdReturnPokemon(int id){
        foreach(var pokemon in PokemonList){
            if (pokemon.id == id) return pokemon;
            
        }
        return null;
    }
}