using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[Serializable]
public class Base
{
    public int HP;
    public int Attack;
    public int Defense;
}
