using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;

public class RandomPokemon : NetworkBehaviour
{
    PokemonArray pa;
    [SerializeField] GameObject Dice;
    [SerializeField] GameObject PlayerInventoriesManager;
    Dice dc;
     PlayersInventories pi;
     Dictionary<string,List<Pokemon>> ListOfPoksOnFields = new Dictionary<string, List<Pokemon>>();
     public Dictionary<string,List<Pokemon>> SynchronizedListOfPoksOnFields = new Dictionary<string, List<Pokemon>>();
     void Start(){
 dc = Dice.GetComponent<Dice>();
        pa = GetComponent<PokemonArray>();
        pi = PlayerInventoriesManager.GetComponent<PlayersInventories>();
        GenerateRandomPokemons();


    }

    void Update()
    {
        if ((IsServer || IsHost))
        {
            
            foreach (var item in ListOfPoksOnFields)
            {
                
                 SynchroPlacedPokemonsClientRpc(item.Key, item.Value[0].id,item.Value[1].id,item.Value[2].id);
                
            }
           
        }
       
       
    }

    public void OnButtonClick(){
        string Tag = dc.Tag;
        
        
        List<Pokemon> PokeList = pa.PokemonList.PokemonList;
        int length = PokeList.Count;
        
        int randomid = Random.Range(0, length);
        
        Pokemon Randompokemon = PokeList[randomid];
        for (int i = 0; i < Randompokemon.type.Length; i++)
        {
            
                
                break;
        
            
        }
        pi.PlayerInventory[dc.TourIterator.Value].AddPokemon(Randompokemon);
        


    }
    [ClientRpc]
    void SynchroPlacedPokemonsClientRpc(string key,int id1,int id2,int id3){
        if (!SynchronizedListOfPoksOnFields.ContainsKey("P40"))
        {
           
            List<Pokemon> temppokemonlist = new List<Pokemon>();
            temppokemonlist.Add(pa.PokemonList.IdReturnPokemon(id1));
            temppokemonlist.Add(pa.PokemonList.IdReturnPokemon(id2));
            temppokemonlist.Add(pa.PokemonList.IdReturnPokemon(id3));
            SynchronizedListOfPoksOnFields.Add(key, temppokemonlist);
        }
    }
    //[ServerRpc(RequireOwnership = false)]
    void GenerateRandomPokemons(){
        List<Pokemon> PokeList = new List<Pokemon>(pa.PokemonList.PokemonList);
          int length = PokeList.Count;
        string[] notbuyable = { "P1", "P4", "P6", "P9", "P11", "P14", "P16","P21", "P24", "P26", "P29", "P31", "P34", "P36" };
        Pokemon Randompokemon;
        List<Pokemon> Randomlist = new List<Pokemon>();
        foreach (var item in pa.FieldType)
        {
            if (!notbuyable.Contains(item.Key))
            { 
                string type = item.Value;
                string typehelper = item.Value;
                if(type=="Normal/Dragon"){
                    type = "Normal";
                    typehelper = "Dragon";
                }else if(type=="Water/Ice"){
                    type = "Water";
                    typehelper = "Ice";
                }
                
                for (int i = 0; i < 3; i++)
                {
                    do
                    {
                        int randomid = Random.Range(0, length);
                        Randompokemon = PokeList[randomid];
                    } while (!Randompokemon.type.Contains(type) && !Randompokemon.type.Contains(typehelper));
                    Randomlist.Add(Randompokemon);
                    PokeList.Remove(Randompokemon);
                    length = PokeList.Count;
                    
                }
                
                ListOfPoksOnFields.Add(item.Key, Randomlist);

                Randomlist = new List<Pokemon>();
                //Debug.Log(Randomlist.Count);
            }
        }
        PokeList = null;
    }
  
}
