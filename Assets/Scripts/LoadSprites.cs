using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class LoadSprites: MonoBehaviour
{
    public List<Sprite> SpriteList;
    Texture2D texture;
    Sprite sprite;
    string filePath = "PokemonSprites/";
    private int totalSprites = 721;
    public float loadDelay = 0.1f;
     public Slider progressBar;
    public TextMeshProUGUI progressText;

    public GameObject Progresspanel;
    void Start(){
        StartCoroutine(LoadSpritesAsync());
        
    }

    IEnumerator LoadSpritesAsync()
    {
        for (int i = 1; i <= totalSprites; i++)
        {
            string spritePath = filePath + i;
            ResourceRequest request = Resources.LoadAsync<Sprite>(spritePath);

            while (!request.isDone)
            {
                float progress = i*100 / totalSprites;
              
                UpdateProgressBar(progress);
                yield return null;
            }

            Sprite sprite = (Sprite)request.asset;
            SpriteList.Add(sprite);

            
           // yield return new WaitForSeconds(loadDelay);
        }

        Progresspanel.SetActive(false);
    }
     void UpdateProgressBar(float progress)
    {
        progressBar.value = progress/100;
        progressText.text = $"Wczytywanie: {Mathf.Round(progress)}%";
    }
        
    
    
    public static Texture2D LoadPNG(string filePath) {

    TextAsset imageAsset = new();
	Texture2D tex = null;
	

	if (File.Exists(filePath)) 	{

		 tex = new Texture2D(2, 2);
        ImageConversion.LoadImage(tex, imageAsset.bytes);
	}
	return tex;
}

}