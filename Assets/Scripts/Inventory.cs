using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class Inventory 
{
    public List<Pokemon> PokemonList = new List<Pokemon>();
    public int NumberOfPokeballs = 3;
      public void AddPokemon(Pokemon pokemon)
    {
        PokemonList.Add(pokemon);
    }
    [ClientRpc]
    public void AddPokeballClientRpc(int numberofpokeballs){
        NumberOfPokeballs += numberofpokeballs;

    }
}