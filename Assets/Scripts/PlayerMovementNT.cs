using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using System.Collections;
public class PlayerMovementNT : NetworkBehaviour
{
    public NetworkVariable<bool> isWalking = new(false); // Czy gracz sie powinien poruszac
    GameObject Player; // GameObject gracza ktory sie bedzie poruszal
    Vector3 MoveToPos; // Vector po ktorym bedzie sie gracz poruszal
    public float speed = 2f; // Predkosc poruszania
    public List<List<bool>> isTaken = new List<List<bool>>(); // Lista 2D sluzaca do okreslenia jakie pola sa zajete i przez ile osob
    string currentTag;
    string TargetTag;
    Dice dice;
    Vector3 currentMoveToPos;

    // Start is called before the first frame update
    void Start()
    {
        dice = GetComponent<Dice>();
            // Po prostu uzupelnienie tej listy false(zadne pole nie jest zajete na poczatku gry),
            // bardzo mozliwe ze powinna byc zsynchronizowana????
        for (int i = 0; i < 40; i++)
        {
            List<bool> row = new List<bool>();
            for (int j = 0; j < 4; j++)
            {
                row.Add(false);

            }
            isTaken.Add(row);
        }


    }
    void Update()
    {
        
        //Sprawdzanie czy powinien sie gracz ruszac, jezeli tak to zaczyna sie ruszac, jezeli bedzie na polu to przestaje
       if (Player is not null)
        {
            int CurrentTagNumber = int.Parse(currentTag.Substring(1));
            int TargetTagNumber = int.Parse(TargetTag.Substring(1));
            //  Debug.Log("iswalking : " + isWalking.Value);
            //  Debug.Log("targettag : " +TargetTag);
            //  Debug.Log("currenttag : " + currentTag);
            //  Debug.Log("CurrentTagNumber<TargetTagNumber : " + (CurrentTagNumber<TargetTagNumber));

            if (isWalking.Value && TargetTag != currentTag && (CurrentTagNumber<TargetTagNumber || 40-CurrentTagNumber+TargetTagNumber<CurrentTagNumber-TargetTagNumber))
            {
            
                if (Vector3.Distance(Player.transform.position, currentMoveToPos) > 0.05f)
                {
                    //Debug.Log("1if: "+currentTag);
                    if (Vector3.Distance(Player.transform.position, currentMoveToPos) < 1.05f && Vector3.Distance(Player.transform.position, currentMoveToPos) >= 0.75f)
                    {
                        MoveToClientRpc(Vector3.MoveTowards(Player.transform.position, new Vector3(currentMoveToPos.x, currentMoveToPos.y + 1f, currentMoveToPos.z), speed * 0.5f * Time.deltaTime));
                    }
                    else if (Vector3.Distance(Player.transform.position, currentMoveToPos) < 0.75f && Vector3.Distance(Player.transform.position, currentMoveToPos) > 0.55f)
                    {
                        MoveToClientRpc(Vector3.MoveTowards(Player.transform.position, currentMoveToPos, speed * Time.deltaTime));
                    }
                    else MoveToClientRpc(Vector3.MoveTowards(Player.transform.position, currentMoveToPos, speed * Time.deltaTime));
                }
                else
                {

                    currentTag = "P" + (CurrentTagNumber + 1).ToString();
                    //Debug.Log("2if: "+currentTag);
                    if (currentTag == "P41") { 
                        dice.mm.ChangePlayerMoneyClientRpc(dice.TourIterator.Value, 300);
                        currentTag = "P1"; }
                    
                    GameObject currentField = GameObject.FindGameObjectWithTag(currentTag);
                    currentMoveToPos = currentField.transform.position;
                }

            }else if(isWalking.Value && TargetTag != currentTag && CurrentTagNumber>TargetTagNumber && 40-CurrentTagNumber+TargetTagNumber>CurrentTagNumber-TargetTagNumber){
                
                if (Vector3.Distance(Player.transform.position, currentMoveToPos) > 0.05f)
                {
                    
                    if (Vector3.Distance(Player.transform.position, currentMoveToPos) < 1.05f && Vector3.Distance(Player.transform.position, currentMoveToPos) >= 0.75f)
                    {
                        MoveToClientRpc(Vector3.MoveTowards(Player.transform.position, new Vector3(currentMoveToPos.x, currentMoveToPos.y + 1f, currentMoveToPos.z), speed * 0.5f * Time.deltaTime));
                    }
                    else if (Vector3.Distance(Player.transform.position, currentMoveToPos) < 0.75f && Vector3.Distance(Player.transform.position, currentMoveToPos) > 0.55f)
                    {
                        MoveToClientRpc(Vector3.MoveTowards(Player.transform.position, currentMoveToPos, speed * Time.deltaTime));
                    }
                    else MoveToClientRpc(Vector3.MoveTowards(Player.transform.position, currentMoveToPos, speed * Time.deltaTime));
                }
                else
                {

                    currentTag = "P" + (CurrentTagNumber - 1).ToString();
                    if (currentTag == "P0") currentTag = "P1";
                    
                    GameObject currentField = GameObject.FindGameObjectWithTag(currentTag);
                    currentMoveToPos = currentField.transform.position;
                }
            }

            if (isWalking.Value && TargetTag==currentTag)
             {
                
                 MoveToClientRpc(Vector3.MoveTowards(Player.transform.position, MoveToPos, speed * Time.deltaTime));      //
             }  

            if (Vector3.Distance(Player.transform.position, MoveToPos) <= 0.05f)
            {
                //Debug.Log("isWalking.Value = false");
                isWalking.Value = false;
            }
                                                                                                                     //
        }                                                                                                              //
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
    IEnumerator WaitUntilArrive(){
        
         while (Player.transform.position!=currentMoveToPos) {
            
            yield return new WaitForSecondsRealtime(1f);
               
                 
            }
        
                
    }
    
    /// <summary>
    /// Zsynchronizowane poruszanie sie gracza danym vektorem
    /// </summary>
    /// <param name="newPosition"></param>
    [ClientRpc]
    public void MoveToClientRpc(Vector3 newPosition)
    {

        if (Player is not null) Player.transform.position = newPosition;


    }
    
    /// <summary>
    /// Zdecydowanie czy gracz powinien sie poruszac i wyznaczenie pozycji na jaka powinien sie poruszac
    /// </summary>
    /// <param name="Tag"></param>
    /// <param name="Player"></param>
    /// <param name="NumberOfPlayers"></param>
    public void MoveTo(string Tag, GameObject Player, int NumberOfPlayers)
    {
        
        int FieldNum = int.Parse(Tag.Substring(1)) - 1;

        GameObject[] Fields = GameObject.FindGameObjectsWithTag(Tag);
        GameObject Field = GameObject.FindGameObjectWithTag(Tag);
        foreach (var obj in Fields)
        {
            if (obj.name == Tag) Field = GameObject.FindGameObjectWithTag(Tag);
            else continue;
        }
       


        Vector3 FieldPos = Field.transform.position;
        Vector3[] Positions = {
      new Vector3(FieldPos.x+0.3f,FieldPos.y,FieldPos.z+0.3f),
      new Vector3(FieldPos.x+0.3f,FieldPos.y,FieldPos.z-0.3f),
      new Vector3(FieldPos.x-0.3f,FieldPos.y,FieldPos.z+0.3f),
      new Vector3(FieldPos.x-0.3f,FieldPos.y,FieldPos.z-0.3f),

     };
    return_:
        for (int i = 0; i < NumberOfPlayers; i++)
        {

            if (!isTaken[FieldNum][i])
            {
                
                isTaken[FieldNum][i] = true;
                MoveToPos = Positions[i];
                this.Player = Player;
                isWalking.Value = true;
                currentTag = Player.tag;
                currentMoveToPos = Player.transform.position;
                TargetTag = Tag;
                break;

            }
            else if (i == NumberOfPlayers-1)
            {
                for (int j = 0; j < 4; j++)
                {
                    isTaken[FieldNum][j] = false;
                    

                }
                goto return_;
            }
        }




    }

}

