using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuickMenu : MonoBehaviour
{
    [SerializeField] GameObject QuickMenuPanel;
    [SerializeField] GameObject DetailEQ;
    [SerializeField] GameObject EqPanel;
    [SerializeField] GameObject MainMenu;
    [SerializeField] Slider slider;
    AudioSource audio;
    float time;
    void Start()
    {

        audio = GetComponent<AudioSource>();
        QuickMenuPanel.SetActive(false);
        audio.volume = 0.3f;
        slider.value = 0.3f;
    }

    // Update is called once per frame
    void Update()
    {
        time -= Time.deltaTime;
        if(DetailEQ.activeSelf)  QuickMenuPanel.SetActive(false);
        if (!DetailEQ.activeSelf)
        {
            if (!QuickMenuPanel.activeSelf && !MainMenu.activeSelf && Input.GetKey(KeyCode.Escape) && time < -0.5f)
            {
                time = 0;
                QuickMenuPanel.SetActive(true);
                EqPanel.SetActive(false);
            }
            else if (Input.GetKey(KeyCode.Escape) && !MainMenu.activeSelf && QuickMenuPanel.activeSelf && time < -0.5f)
            {
                time = 0;
                QuickMenuPanel.SetActive(false);
            }
        }
        else if (Input.GetKey(KeyCode.Escape) && MainMenu.activeSelf)
        {
            Application.Quit();
        }else if(Input.GetKey(KeyCode.Escape)){
            DetailEQ.SetActive(false);
            EqPanel.SetActive(false);
            time = 0;
            
        }
        
        audio.volume = slider.value;
    }
}
