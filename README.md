# Pokepoly
C#  UNITY z użyciem frameworka Unity.Netcode (multiplayer LAN) 

Aby grać przez LAN trzeba być połączonym do jednego routera, lub skorzystać z programów typu Hamachi/Radmin Vpn czy inne tego typu.
Aby uruchomić grę wystarczy uruchomić Pokepoly.exe, następnie jedna osoba musi stworzyć gre(uruchomić serwer) natomiast 
reszta graczy którzy chcą dołączyć musi kliknąć "Dołącz do gry" a następnie wpisać odpowiedni adres IP (lokalny adres Hosta) i kliknąć
"Dołącz".

(Wartości liczbowe mogą być różne, wszystko zależy od balansu rozgrywki) 

Pokepol/Pokepoly to gra planszowa typu Monopol, z połączeniem z pokemonami. 
Gra polega na kupowaniu pól i zbieraniu pokemonów (Maksymalnie 8). 
Zaczynamy z gotówką równą 1000€ , 3 pokeballami i bez żadnego pokemona na polu Start. 

W grze używamy dwóch sześciennych kostek naraz, co daje rozrzut od 2 do 12. 
Jest kilka typów pól : 
- Pola neutralne (Takie pola na których nic się nie dzieje) ilość : 3; 
- Pola PokeCentre (PokeCentre to miejsce w których możemy uleczyć nasze pokemony jak i kupić pokeballe) ilość : 4; 
- Pola żywiołowe (Te pola kupujemy za 150€, pole jest na zawsze naszą własnością. 
Kupując dane pole losujemy 3 pokemony danego żywiołu. Pokemony te są na stałe przypisane do tego pola, 
oraz każdego z nich trzeba kupić osobno za 50€. Każdy gracz który wejdzie na kupione przez nas pole płaci 
stały czynsz 100€.) Jest 12 różnych pól żywiołowych, w czym 14 żywiołów 
([woda i lód] oraz [normalny i smok] są na jednym polu) łączna ilość : 26; 
- Pola Szansy/Wyzwania (Na tych polach losujemy karte szansy lub wyzwania, zależnie od pola.) ilość : 4 
- Pola Nagrody (Pole na którym losujemy ilość gotówki jaką otrzymamy, maksymalnie 900€); ilość : 1 
- Pola Jaskini (Pole na którym tracimy 2 kolejki) ilość : 1 
- Pola Kary (Pole na którym tracimy 150€) ilość : 1 

Łącznie w grze jest 40 pól, co tworzy pole kwadratowe o boku 10 pól. 

W grze dostępny jest ekwipunek w którym jest pokazane ile mamy pokemonów i jakich, oraz ilość pokeballi jaką posiadamy.
W tym ekwipunku możemy kliknąc (Pokaż wiecej), wtedy wyświetlą nam się wszystkie pokemony jakie posiadamy ale z dokładnymi 
statystykami oraz ich wyglądem (spritem).

Gre rozpoczyna Host, osoba która uruchamia Server oraz jest również graczem, kolejność jest identyczna jak kolejność połączenia się 
z serwerem. Pierwsza osoba która się połączyła (czyli Host) to Gracz1, druga Gracz2 etc. 
Maksymalna ilość graczy to 4, minimalna teoretycznie to 1, aczkolwiek z logicznego punktu widzenia 
nie ma sensu grać samemu w planszówke która polega na rywalizacji. W grze niedostępna jest gra z komputerem. 

Celem gry, jest niezbankrutowanie (Tak jak w Monopoly), zatem odpada ten gracz który by grać dalej musiałby się zadłużyć. 
Przykładowo : Mając 100€ wpadlibyśmy na pole Kary, gdybyśmy zapłacili zostałoby nam -50€, w takim wypadku odpadamy. 

W grze jest 721 różnych pokemonów, każdy z własnym Spritem oraz statystykami. 
Statystki pokemona:
- Def (Punkty obrony pokemona)
- Hp (Punkty życia pokemona)
- Atk (Punkty ataku pokemona)

Oryginalna gra Pokepol (fizyczna planszówka) zakłada : 
- Walke z użyciem pokemonów gdy wdepnie się na to samo pole co inny gracz, przegrany płaci x€ wygranemu 
- Poziom pokemona wpływający na jego statystykami 
- Ewolucję pokemona po osiągnieciu danego poziomu 
- Lige Jodo która się zaczyna po X okrążeniach planszy i zebraniu odznak. Wygrany otrzymuje x€ jako nagrodę. 
Są to punkty niezrealizowane w moim projekcie z uwagi na złożoność tych funkcjolaności względem czasu. 

# Techniczny punkt widzenia

Włożyłem w te gre bardzo dużo czasu, myślę, że około 100h aczkolwiek nie liczyłem tego. 
Przełożyło się to na około 2500 lini samego kodu. Jest w nim sporo powtarzalności, wiele rzeczy możnaby 
zrobić dużo lepiej, ładniej i po prostu czytelniej. Niektóre nazwy zmiennych/funkcji mogą być dosyć niezrozumiałe, 
bo przez fakt, że nie było tak dużo czasu, nie skupiałem się na jakości kodu a tylko na tym, żeby działał. 
W wykorzystuje obiektowość języka C# oraz jest kilka funkcji asynchronicznych. 
Sam kod jest podzielony na 21 plików .cs. Do stworzenia multiplayera na różnych urządzeniach skorzystałem z paczki Unity 
UnityNetCode która oferuje dosyć prostą implementacje sieciowej rozgrywki. Dla uproszczenia ograniczyłem się tylko do LAN. 

Problemy tej gry:

- Z powodu, że to pierwsza gra multiplayer jaką w życiu robiłem jest bardzo niestabilna. Rozłączenie się klienta lub hosta może skutkować 
"wysypaniem" się gry, w samej grze nie zdążyłem też zaimplementować zapisu stanu gry, więc obecnie można grać tylko "na raz".

- Nie skupiałem się na grafice ani estetyce więc gra wygląda dosyć słabo ale jest to kwestia zrobienia odpowiednich assetów i podmiany

- Więcej rażących błędów/problemów nie spotkałem, co nie znaczy, że więcej ich nie ma

Bazy danych są w pliku typu .json :
- Challenge.json - Są dane dotyczące kart wyzwania
- Chance.json - Są dane dotyczące kart szansy
- FieldTypes.json - Tutaj jest słownik pól i typu pola. Każde pole ma swój Tag i swój Typ ["Tag" : Typ]
- pokedex.json - Jest to całkiem spora baza danych w której znajduje się 721 pokemonów. Została pobrana z internetu : 
https://github.com/fanzeyi/pokemon.json

Grafiki oraz animacje użyte w projekcie dzielą się na zrobione przeze mnie oraz pobrane z internetu :

- Grafiki pól (Stworzyłem je w Paincie "byle by były". Oprócz grafiki PokeCentre, tą jedyną pobrałem z internetu)

- Sprite'y Pokemonów (Wszystkie sprite'y pobrałem z internetu)

- Animacje (Aniamcje rzutu kostką oraz poruszania się pionka zrobiłem samodzielnie)

- Animowane grafiki (Tło planszy oraz inne animowane grafiki pobrałem z internetu)

- Wszystkie inne grafiki (Karty szansy , wyzwania, menu główne czy inne wykonałem samodzielnie)

Projekt traktowałem bardziej jako zabawę, robiłem to bardziej dla siebie niż myślać, że to zwykły projekt i zamierzam rozbudowywać 
tę gre, żeby mogła pewnego dnia stać się całkiem grywalna i ciekawa.




